import sys
sys.path.append('../pytui/')
import base
import config_screen
import Border
import Label


class Test1():

    #def __init__(self):
    #    super(Test1, self).__init__()

    def run_test1(self):
        border = Border.Border()
        cs = config_screen.Config_Screen()
        cs.set_border(border)

        terminal = base.Terminal(cs)
        terminal.run()

    def run_test2(self):
        border = Border.Border()
        terminal = base.Terminal(Border=border)
        terminal.run()

class Test2():

    # def __init__(self):
    #     super(Test2, self).__init__()

    def run_test1(self):
        label = Label.Label("label1", "Hello World", 1, 1)
        label2 = Label.Label("label2", "Hello World2", 19, 1)

        terminal = base.Terminal()
        terminal.add(label)
        terminal.add(label2)

        terminal.run()

if __name__ == "__main__":
    # test1 = Test1()
    # test1.run_test1()
    # test2 = Test1()
    # test2.run_test2()
    test2 = Test2()
    test2.run_test1()
