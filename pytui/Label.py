class Label(object):

    def __init__(self, id_label, text="", pos_x=0, pos_y=0, attr=None):
        """
        This is a label class. This is used to put text on screen.

        #TODO: The ID label optionally should generate a id, to avoid that
        the devolper set the id obligatory

        #TODO: use attr of curses.

        :param string id_label: The ID of the label object. This is neccesary.
        :param string text: The text that will contain this label object
        :param int pos_x: X position of the label
        :param int pos_y: Y position of the label
        """

        self.id_label = id_label
        self.text = text
        self.x = pos_x
        self.y = pos_y


    def set_text(self, text):
        """
        Set text label

        :param string text: Label's text

        """

        self.text = text

    def set_x_pos(self, x):
        """
        Set x position

        :param int x: X position of the label
        """
        self.x = x

    def set_y_pos(self, y):
        """
        Set y position

        :para int y: Y position of the label
        """
        self.y = y

    def set_x_y_pos(self, x, y):
        """
        Set bot x and y position

        :param int x: X position of the label
        :para int y: Y position of the label
        """
        self.x = x
        self.y = y
