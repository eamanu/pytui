# coding: utf-8
# Created on: 2018-06-01
# Author: Emmanuel Arias
# E-mail: eamanu@eamanu.com
"""
Base of pytui
:author: Emmanuel Arias
"""

import curses
import sys
sys.path.append(".")

from Label import Label
# from config_screen import Config_Screen


class Terminal(object):
    """
    Create the terminal
    """
    def __init__(self,  Border=None):
        """
        This is the constructor

        :param Border Border: This is a border object. If a border object is
        assigned it is assumed thtat the terminal has border.

        """
        # self.CScreen = config_screen
        self.Border = Border
        self.__list_labels = list()

    def kill_terminal(self):
        """
        Kill the Terminal.
        This is replace with the use of wrapper.
        """
        curses.nocbreak()
        self.__screen.keypad(False)
        curses.echo()
        curses.endwin()
        exit(0)

    def ___hello_world(self):
        """
        This is a test function.

        This have to bee remove on prouction
        """
        while 1:
            c = self.__screen.getch()
            if c == ord('q'):
                self.kill_terminal()
            self.__screen.addstr("Hello world\n")
            self.__screen.refresh()
            self.set_border()

    def ___hello_world2(self, screen):
        """
        This is a test function.

        This have to been remove on prouction
        """
        while 1:
            c = screen.getch()
            if c == ord('q'):
                self.kill_terminal()
            screen.addstr('Hello World!\n')
            screen.refresh()
            self.set_border(screen)

    def ___print_labels(self, stdscr):
        """
        This private method print the labels on the screen

        :param curses.stdscr stdscr: The screen used.
        """
        # TODO: used the attr

        for labels in self.__list_labels:
            stdscr.addstr(labels.x, labels.y,
                          labels.text)

    def compile(self, stdscr):
        """
        This compile the window. This function is send to
        curses.wrapper function

        :param curses.initscr stdscr: curses.initscr
        """
        stdscr.clear()
        stdscr.refresh()
        # self.___hello_world2(stdscr)
        if self.Border is not None:
            # Border is used
            self.set_border(stdscr,
                            self.Border.get_parameters())
        while 1:
            # stdscr.clear()
            stdscr.refresh()
            c = stdscr.getch()

            if c == ord('q'):
                return -1

            # Print the elements

            # Print labels
            self.___print_labels(stdscr)

            # curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
            # stdscr.addstr(1, 1, "RED ALERT!", curses.color_pair(1))
        return 0

    def run(self):
        """
        This is the run.
        All The framework need start here.
        """
        curses.wrapper(self.compile)

    def quit_border(self):
        """
        Quit the border of the screen

        Parameters
        ==========
        None
        """
        self.Border = None

    def set_border(self, screen, parameters):
        """
        This is the border function of curses.border
        Draw a border aorund the edges of the window.

        :param curses.screen screen: curses.screen

        :param parameters:
        See below

        :Keyword Arguments:
        :param int ls: Left Side
        :param int rs: Right side
        :param int ts: Top
        :param int bs: Bottom
        :param int tl: Upper-left corner
        :param int tr: Upper-right corner
        :param int bl: Bottom-left corner
        :param int br: Bottom-right corner
        """
        screen.border(parameters['ls'], parameters['rs'],
                      parameters['ts'], parameters['bs'],
                      parameters['tl'], parameters['tr'],
                      parameters['bl'], parameters['br'])

    def add(self, element):
        """
        Method used to add elements to the screen

        :param PyTui object element: A PyTui Object element
        """

        if isinstance(element, Label):
            # Ok, I will add a label element
            # So I will append this to label list
            self.__list_labels.append(element)
